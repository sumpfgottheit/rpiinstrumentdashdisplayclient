__author__ = 'Florian'

import pygame
from pygame import Rect
from pygame.font import Font
from config import *

class Widget(object):
    def __init__(self, surface, x, y, w, h, fill_background=False, draw_borders=False):
        self.surface = surface
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.rect = Rect(self.x, self.y, self.w, self.h)
        self.background_color = BACKGROUND_COLOR
        self.border_color = FOREGROUND_COLOR
        self.fill_background = fill_background
        self.draw_borders = draw_borders

    def draw(self):
        if self.fill_background and not self.draw_borders:
            pygame.draw.rect(self.surface, self.background_color, self.rect, 0)
        elif self.fill_background and self.draw_borders:
            pygame.draw.rect(self.surface, self.background_color, self.rect, 0)
            pygame.draw.rect(self.surface, self.border_color, self.rect, 1)
        elif not self.fill_background and self.draw_borders:
            pygame.draw.rect(self.surface, self.border_color, self.rect, 1)

    @property
    def xx(self):
        return self.x + self.h

    @property
    def yy(self):
        return self.yy + self.h

class TextWidget(Widget):
    def __init__(self, surface, x, y, w, h):
        super(TextWidget, self).__init__(surface, x, y, w, h)
        self.value = ""
        self.fontsize = self.find_font_size()
        self.font = Font(FONT, self.fontsize)
        self.background_color = BACKGROUND_COLOR
        self.font_color = FOREGROUND_COLOR
        self.fill_background = True
        self.align = ALIGN_CENTER

    def update(self, value):
        if value != self.value:
            self.value = value
            return True
        return False

    def draw(self):
        super(TextWidget, self).draw()
        fontsurface = self.font.render(str(self.value), True, (self.font_color))
        fontrect = fontsurface.get_rect()
        fontrect.centery = self.rect.centery
        if self.align == ALIGN_CENTER:
            fontrect.centerx = self.rect.centerx
        elif self.align == ALIGN_LEFT:
            fontrect.x = self.rect.x
        elif self.align == ALIGN_RIGHT:
            fontrect.right = self.rect.right
        return self.surface.blit(fontsurface, fontrect)

    def find_font_size(self):
        size = 100
        while size >= 1:
            f = Font(FONT, size)
            w, h = f.size('8')
            if w < self.w and h < self.h:
                return size
            size = size -1
        return size

#    def draw(self):
#        super(TextWidget, self).draw()
#        text = self.font.render(str(self.value ), True, (self.font_color))
#        textpos = text.get_rect()
#        textpos.centerx = self.rect.centerx
#        textpos.centery = self.rect.centery
#        return self.surface.blit(text, textpos)



class GearNumberWidget(TextWidget):
    def __init__(self, surface, x, y, w, h):
        super(GearNumberWidget, self).__init__(surface, x, y, w, h)

    def update(self, value):
        if value == 0:
            value = 'N'
        elif value == -1:
            value = 'R'
        if value != self.value:
            self.value = value
            return True
        return False

class RPMWidget(TextWidget):
    def __init__(self, surface, x, y, w, h):
        super(RPMWidget, self).__init__(surface, x, y, w, h)

class SpeedWidget(TextWidget):
    def __init__(self, surface, x, y, w, h):
        super(SpeedWidget, self).__init__(surface, x, y, w, h)

    def update(self, value):
        value = int(round(value))
        if value != self.value:
            self.value = value
            return True
        return False