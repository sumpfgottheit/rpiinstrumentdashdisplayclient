#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Based on touchv6, Texy 5/12/13

import pygame, sys, os, time
import pickle
from util import get_lan_ip, update_dict, find_updated_keys
import socket
import select
from pygame.locals import *
from evdev import InputDevice, list_devices
from widgets import *
from config import *
import sys

devices = map(InputDevice, list_devices())
eventX=""
for dev in devices:
    if dev.name == "ADS7846 Touchscreen":
        eventX = dev.fn
print eventX

os.environ["SDL_FBDEV"] = "/dev/fb1"
os.environ["SDL_MOUSEDRV"] = "TSLIB"
os.environ["SDL_MOUSEDEV"] = eventX

pygame.init()

# set up the window
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT), 0, SCREEN_DEEP)

# Fill surface
surface = pygame.Surface(screen.get_size())
surface = surface.convert()
surface.fill(GREY)

widgets = {}

i = 1
for x in (0, 40, 80, 120, 160, 200, 240, 280):
    for y in (0, 40, 80, 120, 160, 200):
        key = "%s%s" % (x, y)
        if i % 4 == 0:
            widget = Widget(surface, x, y, 39, 39, fill_background=False, draw_borders=True)
        elif i % 4 == 1:
            widget = Widget(surface, x, y, 39, 39, fill_background=True, draw_borders=True)
            widget.background_color = GREEN
            widget.border_color = RED
        elif i % 4 == 2:
            widget = Widget(surface, x, y, 39, 39, fill_background=True, draw_borders=False)
            widget.background_color = BLUE
        elif i % 4 == 3:
            widget = Widget(surface, x, y, 39, 390, fill_background=False, draw_borders=False)
        widgets[key] = widget
        i = i + 1

for widget in widgets.values():
    widget.draw()

widgets = {}

widget_gearnumber = GearNumberWidget(surface, 0, 0, 79, 119)
widgets['gear'] = widget_gearnumber

widget_speed = SpeedWidget(surface, 80, 0, 79, 39)
widget_speed.align = ALIGN_RIGHT
widgets['speed'] = widget_speed

label_speed = TextWidget(surface, 160, 20, 39, 19)
label_speed.value = "km/h"
widgets['label_speed'] = label_speed

widget_rpm = RPMWidget(surface, 0, 120, 79, 39)
widgets['rpm'] = widget_rpm

for widget in widgets.values():
    widget.draw()

#
# Set up Networking
#
local_ip = get_lan_ip()
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setblocking(0)
sock.bind((local_ip, UDP_PORT))

data = {}
screen.blit(surface, (0, 0))
pygame.display.flip()
running = True
updated_keys = []
dirty_rects = []

print widgets.keys()

while running:
    #
    # Read from Network
    #
    ready = select.select([sock], [], [], TIMEOUT_IN_SECONDS)
    if ready[0]:
        newdata = sock.recv(BUF_SIZE)  # Recieve from udp
        newdata = pickle.loads(newdata)   # unpickle the data
        del dirty_rects[:]  # Empty the list

        widget_gearnumber.update(newdata['gear'])
        dirty_rects.append( widget_gearnumber.draw() )

        widget_speed.update(newdata['kmh'])
        dirty_rects.append(widget_speed.draw())

        widget_rpm.update(newdata['rpms'])
        dirty_rects.append(widget_rpm.draw())


#        if len(data) == 0:
#            data = newdata
#        updated_keys = find_updated_keys(data, newdata)
#        data = update_dict(data, newdata, updated_keys=updated_keys)
#
#
#    dirty_widgets = []
#    for key in updated_keys:
#        if key in widgets.keys():
#            if widgets[key].update(data[key]):
#                dirty_widgets.append(widgets[key])
#    for widget in dirty_widgets:
#        widget.draw()

    for event in pygame.event.get():
        if event.type == QUIT:
            running = False
            pygame.quit()
            sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
#            print("Pos: %sx%s\n" % pygame.mouse.get_pos())
#            if textpos.collidepoint(pygame.mouse.get_pos()):
            running = False
            pygame.quit()
            sys.exit()
        elif event.type == KEYDOWN and event.key == K_ESCAPE:
            running = False

    screen.blit(surface, (0, 0))
    pygame.display.flip()
    #pygame.display.update()

pygame.quit()